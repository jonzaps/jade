#include "jadeio.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>



int diff(char* file_a, char* file_b)
{
  int f_a = open(file_a, O_RDONLY);

  int f_b = open(file_b, O_RDONLY);

  while(1)
    {
    unsigned char buf_a[1024];

    ssize_t n_a = read(f_a, buf_a, sizeof(buf_a));

    if (!n_a && errno == EINTR)
      continue;

    if (!n_a)				// EOF
      {
      close(f_a);
      close(f_b);
      return 0;				// no differences found
      }

    unsigned char buf_b[1024];

    unsigned char* t = buf_a;
    unsigned char* e = buf_a+n_a;

    while (t < e)
      {
      ssize_t n_b = read(f_b, buf_b, e-t);

      int d = memcmp(t, buf_b, n_b);
      if (d)
	{
        close(f_a);
	close(f_b);
        return d;
	}
      t += n_b;
      }
    }
}

static inline int min(int a, int b)
{ return a < b ? a : b; }

int test1()
{
  srand48(1);

  int f_in = open("data_in", O_RDONLY);

  int f_out = open("./data_out", O_WRONLY | O_CREAT | O_TRUNC, S_IWUSR|S_IRUSR, S_IRGRP|S_IROTH);

  while(1)
    {
    unsigned char mbuf[1024];

    ssize_t nr = read(f_in, mbuf, sizeof(mbuf));

    if (!nr && errno == EINTR)
      continue;

    if (!nr)				// EOF
      break;

    unsigned char* t = mbuf;
    unsigned char* e = mbuf+nr;

    while (t < e)
      {
      int b = (lrand48() >>16) & 0xf;
      ssize_t nw = write(f_out, t, min(b, e-t));
      t += nw;
      }
    }
  close (f_in);
  close (f_out);

  return diff("data_in", "data_out");
}

int test2()
{
  srand48(1);

  int f_in = open("data_in", O_RDONLY);

  jadeio_init(f_in, O_RDONLY);

  int f_out = open("data_out", O_WRONLY);

  jadeio_init(f_out, O_WRONLY);

  while(1)
    {
    jadeio_upkeep_all();

    unsigned char mbuf[1024];

    ssize_t nr = jadeio_read(f_in, mbuf, sizeof(mbuf));

    if (!nr && errno == EINTR)		// doesn't happen
      continue;

    if (!nr)				// EOF
      break;

    unsigned char* t = mbuf;
    unsigned char* e = mbuf+nr;

    while (t < e)
      {
      int b = (lrand48() >>16) & 0xf;
      ssize_t nw = jadeio_write(f_out, t, min(b, e-t));
      t += nw;

      jadeio_upkeep_all();
      }
    }

  jadeio_flush(f_out);
  jadeio_destroy(f_in);
  jadeio_destroy(f_out);

  close (f_in);
  close (f_out);

  return diff("data_in", "data_out");
}


int test3()
{
  srand48(1);

  int f_in = open("data_in", O_RDONLY);

  jadeio_init(f_in, O_RDONLY);

  int f_out = open("data_out", O_WRONLY);

  jadeio_init(f_out, O_WRONLY);

  while(1)
    {
    //jadeio_upkeep_all();

    unsigned char mbuf[1024];

    ssize_t nr = jadeio_read(f_in, mbuf, sizeof(mbuf));

    if (!nr && errno == EINTR)		// doesn't happen
      continue;

    if (!nr)				// EOF
      break;

    unsigned char* t = mbuf;
    unsigned char* e = mbuf+nr;

    while (t < e)
      {
      int b = (lrand48() >>16) & 0xf;
      ssize_t nw = jadeio_write(f_out, t, min(b, e-t));
      t += nw;

      }
    }

  jadeio_flush(f_out);
  jadeio_destroy(f_in);
  jadeio_destroy(f_out);

  close (f_in);
  close (f_out);

  return diff("data_in", "data_out");
}

int main(int argc, char *argv[])
{
  printf("Tests starting\n");
  printf("Test 1:  Direct read/write: %s\n", test1() ? "Failed" : "OK");
  printf("Test 2:  Buffered read/write: %s\n", test2() ? "Failed" : "OK");
  printf("Test 3:  Slow Buffered read/write: %s\n", test3() ? "Failed" : "OK");
}


