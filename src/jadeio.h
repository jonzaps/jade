// This file provides the interface for a buffered version of the
// (normally unbuffered) I/O functions read(), write(), etc.
//
// Contrary to the stdio versions fread() and fwrite(), you get:
// a) The same interface as the original versions, allowing for easy, drop-in
//    replacement.
// b) Much bigger buffers, usable as jitter-buffers etc.
// c) Much more control over buffer behavior.
//
// Note that buffering via this file serves two purposes:
// a) Make two different devices work together that have different speeds,
//    especially in burst.  Examples are writing at a constant rate to the
//    Internet (which can congest momentarily), or reading from the Internet
//    and having to provide a constant output rate (a jitter buffer).
// b) Collect calls to avoid many tiny calls.  E.g. if one called write()
//    1000 times with one char each, the network connection could slow down
//    noticable, since a new packet is sent for every write() and this incurs
//    overhead.  This is what fread/fwrite() uses its buffers for.

// Jadeio can buffer reading, writing, or both.  You need to tell it which
// of these modes you need.

// The file descriptor used by jadeio is the same as the file descriptor
// used by the underying open/read/write mechanism.  All of jadeio's internal
// structured are hidden in process-global memory referenced by that file
// descriptor.


// get MTU:
//   struct ifreq_ifr.
//   ioctl(socket, SIOCGIFMTU, &ifr);
//   int mtu = ifr.ifr_mtu;


#ifndef JADEIO_H
#define JADEIO_H

#include "jade.h"

#include <sys/types.h>




// Initialize a jadeio buffer for file descriptor fd.
// This one uses two Existing jade buffers.
// If either of these is NULL, they are not used.
int jadeio_transform(int fd, jade *read, jade *write);


// initialize jadeio's buffers for file descriptor fd.
// mode is O_RDONLY, O_WRONLY, or O_RDWR, as defined in sys/types.h.
int jadeio_init(int fd, int mode);


// How many bytes are waiting in the buffer.
// type is either O_RDONLY, O_WRONLY, or 0 (only allowed if this fd was
// declared with only a O_RDONLY or O_WRONLY).
ssize_t jadeio_filled(int fd, int type);

// How many bytes are free in the buffer.
// type is either O_RDONLY, O_WRONLY, or 0 (only allowed if this fd was
// declared with only a O_RDONLY or O_WRONLY).
ssize_t jadeio_empty(int fd, int type);


// Performs a buffered read on fd.
ssize_t jadeio_read(int fd, void *buf, size_t count);

// Returns the place in the buffer where readible data is currently stored.
// You may use _filled() bytes from that location.
const void* jadeio_read_target(int fd);

ssize_t jadeio_read_done(int fd, size_t count);


// Performs a buffered write on fd.
ssize_t jadeio_write(int fd, const void *buf, size_t count);

// Returns the place in the buffer where data will be stored.
// You may write _empty() bytes into that location. 
void* jadeio_write_target(int fd);

// Tells the library that count bytes were put into write_target()'s location.
ssize_t jadeio_write_done(int fd, size_t count);



// Perform the necessary actions for the buffers.
// This should be called often during your program.
// This attempts to:
// * write off full blocks in write-buffers until that would block or the
//   buffer is empty (or at least, not enough to fill an mtu size block).
// * read new full blocks in read-buffers until that would either block
//   or the buffer is filled.
void jadeio_upkeep(int fd);

// Peforms upkeep on all open fds.
void jadeio_upkeep_all();

// This sets the max size that the buffer supports.  Note the memory footprint
// is typically larger.
// type as in buffered_open().
// Note that it is possible to call setsize(fd, O_RDONLY, size_read);
// setsize(fd, O_WRONLY, size_write); on a O_RDWR buffer to specify different
// sizes for the in and out buffers.
void jadeio_setsize(int fd, int type, size_t max);

// This sets the minimum size that the buffer normally attempts to read or
// write via the underlying io.
// set max to 0 to ignore mtu behavior.
// Type as in setup().
// Note that it is possible to call setmtu(fd, O_RDONLY, size_read);
// setmtu(fd, O_WRONLY, size_write); on a O_RDWR buffer to specify different
// mtu's for the in and out buffers.
void jadeio_setmtu(int fd, int type, size_t max);


// Write remaining data to the underlying io calls.
// This blocks until all bytes have been written, emptying the buffer.
void jadeio_flush(int fd);

// Destroys this buffer, freeing all memory.
// This does NOT flush.
void jadeio_destroy(int fd);




// Convenience functions

#if 0
// These functions behave exactly as their man 2 counterparts with jadeio_
// missing.  They do, however, include the appropriate jadeio_setup.
int jadeio_open(const char *pathname, int flags, mode_t mode);

int jadeio_creat(const char *pathname, mode_t mode);

int jadeio_openat(int dirfd, const char *pathname, int flags, mode_t mode);

int jadeio_openat2(int dirfd, const char *pathname, const struct open_how *how, size_t size);

// This closes the file and then destroys the jade buffer(s) associated
// with it.
// Note that for a write buffer, this implies a flush and that means it
// can take considerable time to complete.
// For a read buffer, no further reads are done, but data already read
// into the buffer will be lost (since the jade buffer will be destroyed).
// For these reasons, it is usually best to set both read and write
// buffer sizes to 0, handle all data and only THEN close the file.
int close(int fd);

// Note, there is no overload of lseek().  Handle this with care.
#endif

#endif
