#include "jade.h"

#include <sys/mman.h>
#include <linux/mman.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>




jade* jade_create(jade_size_t min_length)
{
  jade* ret = malloc(sizeof(jade));
  if (!ret)
    return ret;
  if(jade_init(ret, min_length) < 0)
    {
    free(ret);
    return NULL;
    }
  return ret;
}
  
int jade_init(jade* buffer, jade_size_t min_length)
{
  memset(buffer, 0, sizeof(jade));

  int ret = jade_capacity_set(buffer, min_length);
  if (ret < 0)
    return ret;
  jade_limit_set(buffer, -1);
  jade_size_set(buffer, min_length);

  return 0;
}

int jade_clear(jade* buffer)
{
  jade_size_t oldlim = jade_limit(buffer);
  munmap(buffer->buffer, (buffer->capacity +1) << 1);
  memset(buffer, 0, sizeof(jade));
  jade_limit_set(buffer, oldlim);
  
  return 0;
}

int jade_destroy(jade** buffer) 
{
  jade_clear(*buffer);
  free(*buffer);
  *buffer = NULL;

  return 0;
}

jade_type* _jade_create_mirror(jade_size_t bs)
{
  // Based on https://github.com/willemt/cbuffer/blob/master/cbuffer.c

  char tmpfile[] = "/tmp/cb-XXXXXX";
  int fd = mkstemp(tmpfile);
  if (fd < 0)
    return NULL;
  int status = unlink(tmpfile);
  if (status)
    return NULL;
  status = ftruncate(fd, bs);
  if (status)
    return NULL;

  /* create the array of data */
  void* b = mmap(NULL, bs<<1, PROT_NONE,
	MAP_ANONYMOUS | MAP_PRIVATE | MAP_UNINITIALIZED, -1, 0);
  if (b == MAP_FAILED)
    return NULL;

  void* b2 = mmap(b, bs, PROT_READ | PROT_WRITE,
        MAP_FIXED | MAP_SHARED, fd, 0);
  if (b2 != b)
    return NULL;
  b2 = mmap(b + bs, bs, PROT_READ | PROT_WRITE,
        MAP_FIXED | MAP_SHARED, fd, 0);
  if (b2 != b + bs)
    return NULL;

  status = close(fd);
  if (status)
    return NULL;

  return (jade_type*) b;
}

jade_size_t jade_capacity_set(jade* buffer, jade_size_t new_cap)
{
  unsigned long long up = sysconf(_SC_PAGESIZE) -1;
  if (new_cap > up)
    up = new_cap;

  // https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
  --up;
  up |= up >>1;
  up |= up >>2;
  up |= up >>4;
  up |= up >>8;
  up |= up >>16;
  up |= up >>32;
  buffer->capacity = up;
  
  jade_type *nb = _jade_create_mirror((up +1) * sizeof(jade_type));
  if (!nb)
    return JADE_MISC;	// most likely OOM

  if (!buffer->buffer)
    buffer->buffer = nb;
  else
    {
    // Need to copy the previous contents first
    jade_size_t f = jade_filled(buffer);
    memcpy(nb, jade_head(buffer), f *sizeof(jade_type));
    buffer->buffer = nb;
    buffer->head = 0;
    buffer->tail = f;
    }

  buffer->capacity = up;
  return buffer->capacity;
}

jade_size_t jade_enqueue(jade* buffer, const jade_type* bytes, jade_size_t amount)
{
  jade_size_t f = jade_free(buffer);
  jade_size_t a = amount < f ? amount : f;

  memcpy(jade_tail(buffer), bytes, a);
  jade_enqueued(buffer, a);

  return a;
}

jade_size_t jade_pop(jade* buffer, jade_type* bytes, jade_size_t amount)
{
  jade_size_t f = jade_filled(buffer);
  jade_size_t a = amount < f ? amount : f;

  memcpy(bytes, jade_tail_read(buffer) -a -1, a);
  jade_popped(buffer, a);

  return a;
}

jade_size_t jade_dequeue(jade* buffer, jade_type* bytes, jade_size_t amount)
{
  jade_size_t f = jade_filled(buffer);
  jade_size_t a = amount < f ? amount : f;

  memcpy(bytes, jade_head(buffer), a);
  jade_dequeued(buffer, a);

  return a;
}

jade_size_t jade_unshift(jade* buffer, const jade_type* bytes, jade_size_t amount)
{
  jade_size_t f = jade_free(buffer);
  jade_size_t a = amount < f ? amount : f;

  memcpy(jade_head_write(buffer) -a -1, bytes, a);
  jade_unshifted(buffer, a);

  return a;
}




// Inline instantiations
jade_size_t jade_filled(jade* buffer);
jade_size_t jade_size(jade* buffer);
int jade_size_set(jade* buffer, jade_size_t newsize);
jade_size_t jade_free(jade* buffer);
jade_size_t jade_limit(jade* buffer);
int jade_limit_set(jade* buffer, jade_size_t newlimit);
int jade_is_empty(jade* buffer);
int jade_is_full(jade* buffer);
jade_size_t jade_capacity(jade* buffer);
jade_size_t jade_limit(jade* buffer);
int jade_limit_set(jade* buffer, jade_size_t max_limit);
jade_size_t jade_reserve(jade* buffer, jade_size_t min_free);
jade_type* jade_tail(jade* buffer);
void jade_enqueued(jade* buffer, jade_size_t amount);
const jade_type* jade_tail_read(jade* buffer);
void jade_popped(jade* buffer, jade_size_t amount);
const jade_type* jade_head(jade* buffer);
void jade_dequeued(jade* buffer, jade_size_t amount);
jade_type* jade_head_write(jade* buffer);
void jade_unshifted(jade* buffer, jade_size_t amount);

