#include "jade.h"
#include "jadeio.h"

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/ioctl.h>
#include <net/if.h>





// a filebuffer
struct fb
{
  int fd;
  jade read;
  int read_mtu;
  jade write;
  int write_mtu;
};

static jade buffers;



// Helper functions
static struct fb* find(int fd)
{
  if (!jade_tail(&buffers))
    return NULL;

  struct fb* a = (struct fb*) jade_head(&buffers);

  for (int r = 0; r < jade_filled(&buffers)/sizeof(struct fb); ++r)
    if (a[r].fd == fd)
      return a+r;

  return NULL;
}

static struct fb* new_fd(int fd)
{
  if (!jade_tail(&buffers))
    jade_init(&buffers, sizeof(struct fb) * 7);

  if (find(fd))
    return NULL;		// FD in use

  jade_reserve(&buffers, sizeof(struct fb));
  struct fb* n = (struct fb*) jade_tail(&buffers);
  memset(n, 0, sizeof(struct fb));
  jade_pushed(&buffers, sizeof(struct fb));

  n->fd = fd;
  return n;
}

int jadeio_transform(int fd, jade *read, jade *write)
{
  struct fb* n = new_fd(fd);
  if (!n)
    return -1;

  if(read)
    n->read = *read;
  if (write)
    n->write = *write;

  return 0;
}



static inline int _jadeio_mtu(int fd)
{
  struct ifreq ifr;
  int ret = ioctl(fd, SIOCGIFMTU, &ifr);
  if (ret < 0)
    return ret;
  return ifr.ifr_mtu;
}

static inline int _jadeio_nonblocking(int fd, int n)
{
  int flags = fcntl(fd, F_GETFL, 0);
  if (flags < 0)
    flags = 0;				// silent failure
  if (n)
    flags |= O_NONBLOCK;
  else
    flags &= ~O_NONBLOCK;
  return fcntl(fd, F_SETFL, flags);
}


// initialize jadeio's buffers for file descriptor fd.
// mode is O_RDONLY, O_WRONLY, or O_RDWR, as defined in sys/types.h.
int jadeio_init(int fd, int mode)
{
  struct fb* n = new_fd(fd);
  if (!n)
    return -1;
  
  if (mode & O_RDONLY)
    {
    n->read_mtu = _jadeio_mtu(fd);
    _jadeio_nonblocking(fd, 1);
    jade_init(&n->read, n->read_mtu);
    }
  if (mode & O_WRONLY)
    {
    n->write_mtu = _jadeio_mtu(fd);
    _jadeio_nonblocking(fd, 1);
    jade_init(&n->write, n->write_mtu);
    }

  return JADE_OK;
}




ssize_t jadeio_filled(int fd, int type)
{
  struct fb* it = find(fd);

  if (!it)
   return 0;

  if (type == O_RDONLY)
    return jade_filled(&it->read);
  if (type == O_WRONLY)
    return jade_filled(&it->write);

  if (!type)
    {
    // TODO missing some error checking here
    if (jade_tail(&it->read))
      return jade_filled(&it->read);
    else
      return jade_filled(&it->write);
    }

  return 0;
}

ssize_t jadeio_free(int fd, int type)
{
  struct fb* it = find(fd);

  if (!it)
   return 0;

  if (type == O_RDONLY)
    return jade_free(&it->read);
  if (type == O_WRONLY)
    return jade_free(&it->write);

  if (!type)
    {
    // TODO missing some error checking here
    if (jade_tail(&it->read))
      return jade_free(&it->read);
    else
      return jade_free(&it->write);
    }

  return 0;
}


ssize_t jadeio_read(int fd, void *buf, size_t count)
{
  struct fb* it = find(fd);
  if (!it)
    return -1;

  return jade_shift(&it->read, buf, count);
}

// Returns the place in the buffer where readible data is currently stored.
// You may use _filled() bytes from that location.
const void* jadeio_read_target(int fd)
{
  struct fb* it = find(fd);
  if (!it)
    return NULL;

  return jade_head(&it->read);
}  

ssize_t jadeio_read_done(int fd, size_t count)
{
  struct fb* it = find(fd);
  if (!it)
    return -1;

  size_t filled = jade_filled(&it->read);
  if (count > filled)
    count = filled;
  jade_shifted(&it->read, count);
  return count;
}


ssize_t jadeio_write(int fd, const void *buf, size_t count)
{
  struct fb* it = find(fd);
  if (!it)
    return -1;

  return jade_push(&it->write, buf, count);
}

void* jadeio_write_target(int fd)
{
  struct fb* it = find(fd);
  if (!it)
    return NULL;

  return jade_tail(&it->write);
}

ssize_t jadeio_write_done(int fd, size_t count)
{
  struct fb* it = find(fd);
  if (!it)
    return -1;

  jade_reserve(&it->write, count);
  // TODO fail check here
  jade_pushed(&it->write, count);
  return count;
}


void jadeio_upkeep(int fd)
{
  if (!jade_tail(&buffers))
    return;

  struct fb* it = find(fd);
  if (!it)
    return;

  if (jade_head(&it->write))
    {
    while (1)
      {
      int avail = jade_filled(&it->write);
      if (avail < it->write_mtu)
	break;		// "empty" -> done
      int sending = (avail / it->write_mtu ) * it->write_mtu;

      ssize_t r = write(fd, jade_head(&it->write), sending);
      if (r < 0)
        {
        if (errno == EINTR)
	  continue;

        if (errno == EAGAIN || errno == EWOULDBLOCK)
	  break;	// "pipeline filled" -> done

        // TODO:  Add fatal error handling here.
        break;
        }

      jade_shifted(&it->write, r);
      }
    }


  if (jade_tail(&it->read))
    {
    while (1)
      {
      jade_size_t res = jade_reserve(&it->read, it->read_mtu);
      if (res < it->read_mtu)
	break;		// Buffer is full
      ssize_t r = read(fd, jade_tail(&it->read), it->read_mtu);
      if (r < 0)
        {
        if (errno == EINTR)
	  continue;

        if (errno == EAGAIN || errno == EWOULDBLOCK)
	  break;	// "pipeline empty" -> done

        // TODO:  Add fatal error handling here.
        break;
        }

      jade_pushed(&it->read, r);
      }
    }
}

void jadeio_upkeep_all()
{
  if (!jade_head(&buffers))
    return;

  struct fb* a = (struct fb*) jade_head(&buffers);

  for (int r = 0; r < jade_filled(&buffers)/sizeof(struct fb); ++r)
    jadeio_upkeep(a[r].fd);
}



void jadeio_setlimit(int fd, int type, size_t max)
{
  struct fb* it = find(fd);
  if (!it)
    return;

  if (type | O_RDONLY)
    jade_limit_set(&it->read, max);
  if (type | O_WRONLY)
    jade_limit_set(&it->write, max);
}

void jadeio_setmtu(int fd, int type, size_t max)
{
  struct fb* it = find(fd);
  if (!it)
    return;

  if (type | O_RDONLY)
    it->read_mtu = max;
  if (type | O_WRONLY)
    it->write_mtu = max;
}


// Write remaining data to the underlying io calls.
// This blocks until all bytes have been written, emptying the buffer.
void jadeio_flush(int fd)
{
  if (!jade_tail(&buffers))
    return;

  struct fb* it = find(fd);
  if (!it)
    return;

  if (jade_head(&it->write))
    {
    while (1)
      {
      int avail = jade_filled(&it->write);
      if (!avail)
	break;				// flushed all
      int sending = avail;

      ssize_t r = write(fd, jade_head(&it->write), sending);
      if (r < 0)
        {
        if (errno == EINTR)
	  continue;

        if (errno == EAGAIN || errno == EWOULDBLOCK)
	  {
          const struct timespec smalldelay = {0, 1000000000L/1000 * 10}; // 10ms
          // Need to try again in a moment.  Avoid busy wait though.
	  nanosleep(&smalldelay, NULL);
          continue;
          }

        // TODO:  Add fatal error handling here.
        break;
        }

      jade_shifted(&it->write, r);
      }
    }
}

// Destroys this buffer, freeing all memory.
// This does NOT flush.
void jadeio_destroy(int fd)
{
  struct fb* it = find(fd);
  if (!it)
    return;

  if (jade_head(&it->write))
    jade_clear(&it->write);
  if (jade_tail(&it->read))
    jade_clear(&it->read);

  // TODO free the fb struct itself
}


// Convenience functions
#if 0

// These functions behave exactly as their man 2 counterparts with jadeio_
// missing.  They do, however, include the appropriate jadeio_init.
int jadeio_open(const char *pathname, int flags, mode_t mode);

int jadeio_creat(const char *pathname, mode_t mode);

int jadeio_openat(int dirfd, const char *pathname, int flags, mode_t mode);

int jadeio_openat2(int dirfd, const char *pathname, const struct open_how *how, size_t size);

// This closes the file and then destroys the jade buffer(s) associated
// with it.
// Note that for a write buffer, this implies a flush and that means it
// can take considerable time to complete.
// For a read buffer, no further reads are done, but data already read
// into the buffer will be lost (since the jade buffer will be destroyed).
// For these reasons, it is usually best to set both read and write
// buffer sizes to 0, handle all data and only THEN close the file.
int close(int fd);

// Note, there is no overload of lseek().  Handle this with care.
#endif
