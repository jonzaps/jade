// This file provides the interface for a fast deque.
// The implementation is based on a growing circular buffer.
// The buffer does include mirroring.  Thus, the entire used area of the
// buffer appears as a single, contiguous area.  Likewise for the free area.
// Also, the buffer is lockfree in the sense that readers and writers can
// act simultaneously (if not using resize/reserve etc.).

// Sizes:
// The buffer knows several distinct sizes.  The names are partially inspired
// by C++ vector<>:
// * FILLED   : # of valid data bytes waiting in the buffer.  This is an
//              invariant, the only way to decrease/increase this number is to
//		read/write actual data out of/into the buffer (e.g. via shift/
//		push).
// * SIZE     : The target size of the buffer.  If FILLED >= SIZE, the buffer
//		is full and further writing fails.  SIZE may be any value
//		within 0 <= SIZE <= LIMIT.  In particular, it is legal to
//		set this SIZE to a value < FILLED.  This preserves the existing
//		data in the buffer, but does not allow further writing until
//		the overflow has been read off.
// * FREE     : # of bytes that may be written into this buffer before the
//		buffer overflows.  Note that this may be negative if SIZE is
//		smaller than FILLED.
//		SIZE = FILLED + FREE
// * LIMIT    : A limit specified for the buffer.  Ordinary resize operations
//		fail when SIZE > LIMIT.  Note that it is easily possible to
//		change the limit via jade_limit_set(), though.
// * CAPACITY : The maximum size that SIZE may reach without the buffer having
//		to reallocate.  This is always a power of 2 -1.
// It is usually best to avoid setting SIZE to a power of 2 because other-
// wise a lot of memory will be wasted.

// Return values
// If a return specifically requests information, that is generally returned,
// with negative values typically indicating an error.  In the case of
// allocation methods, NULL is returned for errors.
// Most other functions return a status value.  If that is >= 0, the call
// worked as expected, if it is <0, this is an error code.
// It is not advisable to use the biggest bit in sizes since you would not
// be able to tell when there are errors (the sizes reported appear negative).


#ifndef JADE_H
#define JADE_H


// This uses 32bit sizes for buffers up to 2GiB.  If you do need more,
// just define this to long long.
typedef long jade_size_t; 			// has to be a signed type
typedef unsigned char jade_type;

// mostly https://stackoverflow.com/questions/2053843/min-and-max-value-of-data-type-in-c/7266266#7266266
#define JADE_ISSIGNED(t) (((t)(-1)) < ((t) 0))
#define JADE_UMAXOF(t) ((t) -1)
#define JADE_SMAXOF(t) ((t)(((0x1ULL << ((sizeof(t) * 8ULL) - 1ULL)) - 1ULL) | \
                    (0x7ULL << ((sizeof(t) * 8ULL) - 4ULL))))
#define JADE_MAXOF(t) ((t)((unsigned long long) (JADE_ISSIGNED(t) ? JADE_SMAXOF(t) : JADE_UMAXOF(t))))


#define JADE_OK (0)
#define JADE_OOM (-1)
#define JADE_RANGE (-2)
#define JADE_MISC (-3)


// The circular buffer.
// Consider these members to be private, manipulate via the functions below.
typedef struct
{
  jade_type* buffer;	// The buffer space
			// Area is [0:(capacity+1)*2] (due to mirroring).
  jade_size_t tail;	// The end of the line counter (write here)
  jade_size_t head;	// The start of the line counter (read here)

  jade_size_t size;	// The current allowable size of the buffer
  jade_size_t limit;	// Maximum that size may be set to
  jade_size_t capacity; // This many bytes currently fit
} jade;




// CREATE/DESTROY methods

// Make an initialized jade object.
jade* jade_create(jade_size_t min_length);

// Initialize an existing jade object.
// Do not use this on an initialized jade object lest there be memory leaks.
int jade_init(jade* buffer, jade_size_t min_length);

// Deinitialize an initialized jade object.
// Frees associated memory and forgets all data (except limit).
int jade_clear(jade* buffer);

// Destroy an initialized or uninitialized jade object.
// Frees associated memory, forgets all data, frees the jade object itself.
// Finally sets *buffer to NULL.
int jade_destroy(jade** buffer);


// SIZE methods

// Report the CAPACITY of the jade buffer.
// Size may become this big before new memory needs to be allocated.
inline jade_size_t jade_capacity(jade* buffer)
{
  return buffer->capacity;
}

// Change the CAPACITY and allocation size of this jade buffer.
// This is the only way to shrink the buffer footprint.
// Returns jade_capacity() or error.
jade_size_t jade_capacity_set(jade* buffer, jade_size_t new_cap);

// Report the SIZE of the jade buffer.
// Note this may be negative.
inline jade_size_t jade_size(jade* buffer)
{
  return buffer->size;
}

// Report the LIMIT of the jade buffer.
inline jade_size_t jade_limit(jade* buffer)
{
  return buffer->limit;
}

// Change the SIZE of the jade buffer.
// This defines how many items at most are allowed in the buffer right now.
// Setting this new size to a value < filled indicates that no new
// pushes should be accepted until the new n is reached.  0 is allowable.
inline int jade_size_set(jade* buffer, jade_size_t newsize)
{
  if (0 > newsize || newsize > jade_limit(buffer))
    return JADE_RANGE;

  if (newsize > buffer->capacity)
    {
    int ret = jade_capacity_set(buffer, newsize);
    if (ret < 0)
      return ret;
    }

  buffer->size = newsize;
  return 0;
}


// Change the LIMIT of the jade buffer.
inline int jade_limit_set(jade* buffer, jade_size_t newlimit)
{
  if (newlimit < 0)
    newlimit = JADE_MAXOF(jade_size_t);
  if (newlimit < jade_size(buffer))
    jade_size_set(buffer, newlimit);
  buffer->limit = newlimit;
  return 0;
}







// CURRENT STATE methods

// This many bytes can be read at this time.
inline jade_size_t jade_filled(jade* buffer)
{
  return (buffer->tail - buffer->head) & buffer->capacity;
}

// This many bytes can be written at this time.
// If you need more, use jade_reserve().
inline jade_size_t jade_free(jade* buffer)
{
  return jade_size(buffer) - jade_filled(buffer);
}

inline int jade_is_empty(jade* buffer)
{
  return jade_filled(buffer) == 0;
}

inline int jade_is_full(jade* buffer)
{
  return jade_filled(buffer) >= jade_size(buffer);
}

// Ensure that this much space is left to write.
// Resizes the buffer if necessary.  If min_free < free, nothing happens.
// Answers the free bytes, so jade_free(b) == jade_reserve(b, 0)
inline jade_size_t jade_reserve(jade* buffer, jade_size_t min_free)
{
  jade_size_t f = jade_free(buffer);
  if (min_free > f)
    {
    int ret = jade_size_set(buffer, jade_size(buffer) + min_free - f);
    if (ret < 0)
      return f;
    else
      return jade_free(buffer);		// Probably better to return min_free
    }
    
  return f;
}


// TAIL methods

// ENQUEUE/PUSH

// Report the TAIL position.
// This is the END of the queue +1 (next free slot to be filled).  
// tail[0:free()] is a valid write area.
// Later, use enqueued() to inform jade about the write.
// The returned pointer is valid until the next jade_ call on this buffer.
inline jade_type* jade_tail(jade* buffer)
{
  return buffer->buffer + buffer->tail;
}

// This many bytes were written to tail.
// Amount needs to be <= jade_free().
inline void jade_enqueued(jade* buffer, jade_size_t amount)
{
  // TODO:  check free()
  buffer->tail = (buffer->tail + amount) & buffer->capacity;
}

// Convenience function.
// Write "amount" bytes, starting at "bytes", to tail.
// Returns the number of bytes actually written.
jade_size_t jade_enqueue(jade* buffer, const jade_type* bytes, jade_size_t amount);

#define jade_pushed jade_enqueued
#define jade_push jade_enqueue

// POP

// Report the TAIL READING position.
// This is the END of the queue +1 (next free slot to be filled).  
// This is the tail to use for reading (as in pop()).
// tail_read[-1-filled():-1] is a valid read area.
// The returned pointer is valid until the next jade_ call on this buffer.
inline const jade_type* jade_tail_read(jade* buffer)
{
  return buffer->buffer + buffer->tail + buffer->capacity +1;
}

// This many bytes were read off tail_read.
// Note that the area affected is [tail -1 -amount :tail -1];
// Amount needs to be <= jade_filled().
inline void jade_popped(jade* buffer, jade_size_t amount)
{
  // TODO:  check filled()
  buffer->tail = (buffer->tail - amount) & buffer->capacity;
}

// Convenience function.
// Read "amount" bytes from the buffer tail into "bytes". 
// Returns the number of bytes actually read.
jade_size_t jade_pop(jade* buffer, jade_type* bytes, jade_size_t amount);



// HEAD methods

// DEQUEUE/SHIFT

// Report the HEAD position.
// This is the start of the queue (item waiting the longest).
// head[0:filled()] is a valid read area.
// Later, use dequeued() to inform jade about the read.
// The returned pointer is valid until the next jade_ call on this buffer.
inline const jade_type* jade_head(jade* buffer)
{
  return buffer->buffer + buffer->head;
}

// This many bytes were read off head.
// Amount needs to be <= filled().
inline void jade_dequeued(jade* buffer, jade_size_t amount)
{
  buffer->head = (buffer->head + amount) & buffer->capacity;
}

// Convenience function.
// Read "amount" bytes from the buffer into "bytes".
// Returns the number of bytes actually read.
jade_size_t jade_dequeue(jade* buffer, jade_type* bytes, jade_size_t amount);

#define jade_shifted jade_dequeued
#define jade_shift jade_dequeue

// UNSHIFT

// Report the HEAD position.
// This is the start of the queue (item waiting the longest).
// This is the head to use for writing (as in unshift()).
// head[-1-free():-1] is a valid write area.
// Later, use unshifted() to inform jade about the write.
// The returned pointer is valid until the next jade_ call on this buffer.
inline jade_type* jade_head_write(jade* buffer)
{
  return buffer->buffer + buffer->head + buffer->capacity +1;
}

// This many bytes were written before head.
// Amount needs to be <= free().
inline void jade_unshifted(jade* buffer, jade_size_t amount)
{
  buffer->head = (buffer->head - amount) & buffer->capacity;
}

// Convenience function.
// Write "amount" bytes, starting from "bytes", to just before head.
// Returns the number of bytes actually written.
jade_size_t jade_unshift(jade* buffer, const jade_type* bytes, jade_size_t amount);

#endif
