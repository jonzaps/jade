# Jade

Jon's Array DEque provides an optimized character deque (a.k.a. double-ended queue) in C.

I recommend reading the wikipedia article https://en.wikipedia.org/wiki/Double-ended_queue on the subject.  Summary:  A deque is a data structure that generally allows O(1) complexity adds/removes to/from both ends.  (Resizing takes longer, of course.)

This is implemented via a growing circular buffer, see https://en.wikipedia.org/wiki/Circular_buffer.

Naming convention:
1. We adopt the naming convention that is analogous to a queue of humans waiting at a counter.  Therefore, new elements are normally added to the TAIL of the queue, old element are removed from the head of the queue.
2. Queues have well-known naming conventions, i.e. adding an element to the tail is called ENQUEUE, removing an element from the head is called DEQUEUE.
3. Stacks have well-known naming conventions, i.e. adding an element to the tail is called PUSH, removing one from the tail is called POP.
back of a queue 
4. Also, we'll support the naming convention of perl and JavaScript, which happen to agree on these operation names.
5. We do allow some aliases.

Therefore, the four most relevant operations are:
add at HEAD	-> unshift
remove at HEAD	-> dequeue/shift
add at TAIL	-> enqueue/push
remove at TAIL	-> pop
